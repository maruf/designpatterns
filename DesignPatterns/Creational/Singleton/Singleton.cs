﻿using System;
using System.Collections.Generic;

namespace DesignPatterns.Creational.Singleton
{
    public sealed class Singleton
    {   
        //initialized by static constructor/type initializer, init once per AppDomain, thread-safe by CLR 
        public static readonly Singleton Instance = new Singleton();

        //instance  
        public DateTime Date=DateTime.Now; // a value-type
        public string Name = "Maruf"; // a ref-type
        public IList<string> Collection=new List<string>(); // a collection ref-type

        //prevent instantiation
        private Singleton   () {}
    }
}
