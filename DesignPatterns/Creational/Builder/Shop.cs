﻿namespace DesignPatterns.Creational.Builder
{
    /// <summary>
    /// The Director
    /// </summary>
    public class Shop
    {
        private VehicleBuilder _builder;

        public void Construct(VehicleBuilder builder)
        {
            _builder = builder;
            _builder.BuildDoors();
            _builder.BuildEngine();
            _builder.BuildFrame();
            _builder.BuildWheels();
        }

    }
}
