﻿namespace DesignPatterns.Creational.Builder
{
    /// <summary>
    /// The Builder interface
    /// </summary>
    public abstract class VehicleBuilder
    {
        protected Vehicle Vehicle;

        public abstract void BuildFrame();
        public abstract void BuildWheels();
        public abstract void BuildDoors();
        public abstract void BuildEngine();

        public Vehicle GetResult()
        {
            return Vehicle;
        }
    }

}
