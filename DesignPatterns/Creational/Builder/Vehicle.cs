﻿using System;
using System.Collections.Generic;

namespace DesignPatterns.Creational.Builder
{
    /// <summary>
    /// the Product
    /// </summary>
    public class Vehicle
    {
        private readonly string _type;
        private readonly IDictionary<string, string> _parts=new Dictionary<string, string>();
        
        public Vehicle(string type)
        {
            _type = type;
        }

        public string this[string key]
        {
            get { return _parts[key]; }
            set { _parts[key] = value; }
        }

        public override string ToString()
        {
            var str=String.Format("Vehicle Type: {0, -15}",  _type);
            
            foreach (var part in _parts)
            {
                str += String.Format("{0}: {1, -15}", part.Key, part.Value);
            }

            return str;
        }

    }
}
