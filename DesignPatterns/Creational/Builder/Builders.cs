﻿namespace DesignPatterns.Creational.Builder
{
    public class CarBuilder:VehicleBuilder
    {
        public CarBuilder()
        {
            Vehicle = new Vehicle("Car");
        }

        public override void BuildFrame()
        {
            Vehicle["frame"] = "Car frame";
        }

        public override void BuildWheels()
        {
            Vehicle["wheels"] = "4";
        }

        public override void BuildDoors()
        {
            Vehicle["doors"] = "4";
        }

        public override void BuildEngine()
        {
            Vehicle["engine"] = "2500 cc";
        }
    }

    public class MotorcycleBuilder : VehicleBuilder
    {
        public MotorcycleBuilder()
        {
            Vehicle = new Vehicle("MotorCycle");
        }
        

        public override void BuildFrame()
        {
            Vehicle["frame"] = "Motorcycle frame";
        }

        public override void BuildWheels()
        {
            Vehicle["wheels"] = "2";
        }

        public override void BuildDoors()
        {
            Vehicle["doors"] = "0";
        }

        public override void BuildEngine()
        {
            Vehicle["engine"] = "250 cc";
        }
    }

    public class ScooterBuilder : VehicleBuilder
    {
        public ScooterBuilder()
        {
            Vehicle = new Vehicle("Scooter");
        }

        public override void BuildFrame()
        {
            Vehicle["frame"] = "Scooter frame";
        }

        public override void BuildWheels()
        {
            Vehicle["wheels"] = "3";
        }

        public override void BuildDoors()
        {
            Vehicle["doors"] = "0";
        }

        public override void BuildEngine()
        {
            Vehicle["engine"] = "500 cc";
        }        
    }
}
