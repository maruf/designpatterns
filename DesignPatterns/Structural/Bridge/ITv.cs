﻿namespace DesignPatterns.Structural.Bridge
{
    //the Implementor
    public interface ITv
    {
        void TurnOn();
        void TurnOff();
        void TuneChannel();
    }    
}
