﻿using System;

namespace DesignPatterns.Structural.Bridge
{
    //different Implementations

    public class SonyTv : ITv
    {
        public void TurnOn()
        {
            Console.WriteLine("Sony specific On");
        }

        public void TurnOff()
        {
            Console.WriteLine("Sony specific Off");
        }

        public void TuneChannel()
        {
            Console.WriteLine("Sony specific Channel tune");
        }
    }

    public class LGTv : ITv
    {
        public void TurnOn()
        {
            Console.WriteLine("LG specific On");
        }

        public void TurnOff()
        {
            Console.WriteLine("LG specific Off");
        }

        public void TuneChannel()
        {
            Console.WriteLine("LG specific Channel tune");
        }
    }
}
