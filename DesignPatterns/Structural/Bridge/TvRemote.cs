﻿namespace DesignPatterns.Structural.Bridge
{
    //the refined abstraction
    public class TvRemote
    {
        private bool _isTvTurnedOn = false;

        public TvRemote(ITv tv)
        {
            _tv = tv;   
        }

        private ITv _tv;

        public void SetImplementor(ITv tv)
        {
            _tv = tv;
            _isTvTurnedOn = false;
        }

        public void ToggleOnOff()
        {
            if(_isTvTurnedOn)
                _tv.TurnOff();
            else
                _tv.TurnOn();

            _isTvTurnedOn = !_isTvTurnedOn;
        }        
    }
}
