﻿namespace DesignPatterns.Structural.Adapter
{
    //the Adapter
    public class AdapterToLegacyApi : NewApi
    {
        private readonly LegacyApi _adaptee=new LegacyApi();

        public override string GetMeSomething()
        {
            _adaptee.DomSomething();

            return _adaptee.ValuableInfo;
        }
    }
}
