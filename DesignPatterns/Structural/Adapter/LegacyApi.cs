﻿namespace DesignPatterns.Structural.Adapter
{
    //the Adaptee
    internal class LegacyApi
    {
        public void DomSomething()
        {
            ValuableInfo = "valuable info from old API";
        }

        public string ValuableInfo { get; private set; }
    }
}
