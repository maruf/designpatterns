﻿namespace DesignPatterns.Structural.Adapter
{
    //the Target
    public class NewApi
    {
        public virtual string GetMeSomething()
        {
            return "valuable info from: new API";
        }
    }
}
