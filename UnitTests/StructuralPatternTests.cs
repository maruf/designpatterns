﻿using System;
using DesignPatterns.Structural.Adapter;
using DesignPatterns.Structural.Bridge;
using NUnit.Framework;

namespace UnitTests
{
    [TestFixture]
    internal class StructuralPatternTests
    {
        [Test]
        public void AdapterPattern_Scenario1()
        {            
            NewApi target=new NewApi();            
            NewApi adapter=new AdapterToLegacyApi();

            Console.WriteLine(target.GetMeSomething());
            //call target method so that the adapter delegates the call to adaptee
            Console.WriteLine(adapter.GetMeSomething());
        }

        [Test]
        public void BridgePattern_Scenario1()
        {
            //create the abstraction passing the implementor
            var tvRemote = new TvRemote(new SonyTv());

            tvRemote.ToggleOnOff();

            //change implementor to new one
            tvRemote.SetImplementor(new LGTv());
            tvRemote.ToggleOnOff();
            tvRemote.ToggleOnOff();
        }
    }
}
