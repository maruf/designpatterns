﻿using System;
using DesignPatterns.Creational.Builder;
using DesignPatterns.Creational.Singleton;
using NUnit.Framework;

namespace UnitTests
{
    [TestFixture]
    internal class CreationalPatternsTests
    {
        [Test]
        public void BuilderScenario1()
        {
            var director = new Shop();
            VehicleBuilder builder = new CarBuilder();
            director.Construct(builder);
            Console.WriteLine(builder.GetResult());
            Console.WriteLine("=======");

            builder = new MotorcycleBuilder();
            director.Construct(builder);
            Console.WriteLine(builder.GetResult());
            Console.WriteLine("=======");

            builder = new ScooterBuilder();
            director.Construct(builder);
            Console.WriteLine(builder.GetResult());
            Console.WriteLine("=======");
        }

        [Test]
        public void SingletonTest()
        {
            var s = Singleton.Instance;
            var s1 = Singleton.Instance;

            Assert.AreSame(s, s1);
            Assert.AreEqual(s.Date, s1.Date);
            Assert.AreSame(s.Name, s1.Name);
            Assert.AreSame(s.Collection, s1.Collection);
        }


        
    }
}
